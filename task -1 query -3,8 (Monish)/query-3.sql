/*c. Display the studentid, studentname and total number of courses registered by that student.*/

/*stud id is in student , studname is in  applicant course num in registration */
/*select a.studentid, count(b.courseid) as total from student a 
inner join registration b on a.studentid=b.studentid 
group by a.studentid;*/

select a.studentid, b.applicantname, count(c.courseid) as Total_num_of_courses from student a, applicant b, registration c
where a.applicantid=b.applicantid and a.studentid=c.studentid group by a.studentid;