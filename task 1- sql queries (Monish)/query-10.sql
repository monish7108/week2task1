/*  j. Display the branchname and the total number of students present in each branch.*/

select branch.branchname,
   COALESCE(count(student.studentid),0) as num_of_students
   from branch  inner join student
   on branch.branchid = student.branchid
   group by branch.branchid;