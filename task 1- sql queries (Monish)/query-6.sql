/*f. Display the department name and total number of instructors in each department.
 If no instructor is available display 0 in the place of total number of instructors.*/

/* Problem in second statement	*/
select dept.departmentname,
   COALESCE(count(inst.instructorid),0) as numb_of_instructors
   from department dept  inner join instructor inst
   on dept.departmentid = inst.departmentid
   group by dept.departmentid;