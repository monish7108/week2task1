/*h. Display the details of the hostel which has maximum allocation.*/


select hostelid,roomsavailable,hosteltype,
hostelfee,max(counted) from (
select hostel.hostelid,hostel.roomsavailable,hostel.hosteltype,
hostel.hostelfee
                                ,count(hostelallocation.hostelid)as counted
from hostelallocation,hostel
where hostel.hostelid=hostelallocation.hostelid
group by hostelid
)as counts ;
