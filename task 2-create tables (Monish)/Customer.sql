create table CUSTOMER (CUST_ID int(5) primary key  check (CUST_ID>=100 and CUST_ID<10000) , CUST_NAME VARCHAR(20),ANNUAL_REVENUE INT(10) default 20000,CUST_TYPE varchar(20));

insert into CUSTOMER (CUST_ID,CUST_NAME,ANNUAL_REVENUE,CUST_TYPE) values(100,'Revathi',500000,'manufacturer');
insert into CUSTOMER (CUST_ID,CUST_NAME,ANNUAL_REVENUE,CUST_TYPE) values(101,'Richa',1800000,'wholesaler');
insert into CUSTOMER (CUST_ID,CUST_NAME,ANNUAL_REVENUE,CUST_TYPE) values(102,'Rishi',1000000,'retailer');
insert into CUSTOMER (CUST_ID,CUST_NAME,ANNUAL_REVENUE,CUST_TYPE) values(103,'Rajesh',4000000,'wholesaler');
insert into CUSTOMER (CUST_ID,CUST_NAME,ANNUAL_REVENUE,CUST_TYPE) values(104,'Kalyan',24800000,'wholesaler');

select * from CUSTOMER;