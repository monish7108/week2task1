create table CITY (CITY_NAME varchar(20) primary key, POPULATION Int(15));

insert into CITY(CITY_NAME,POPULATION) values('London',100000000);
insert into CITY(CITY_NAME,POPULATION) values('Paris',120000000);
insert into CITY(CITY_NAME,POPULATION) values('Rome',200000000);
insert into CITY(CITY_NAME,POPULATION) values('Panama City',1230000000);
insert into CITY(CITY_NAME,POPULATION) values('San Francisco',20000000);

select * from CITY;