create table TRUCK (TRUCK_NO INT(3) primary key, DRIVER_NAME varchar(20));

insert into TRUCK(TRUCK_NO,DRIVER_NAME) values(100,'Jensen');
insert into TRUCK(TRUCK_NO,DRIVER_NAME) values(101,'Sasi');
insert into TRUCK(TRUCK_NO,DRIVER_NAME) values(102,'Jake Stinson');
insert into TRUCK(TRUCK_NO,DRIVER_NAME) values(103,'Alex');

select * from TRUCK;