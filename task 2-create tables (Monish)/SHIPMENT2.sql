alter table SHIPMENT add foreign key (TRUCK_NO) references TRUCK(TRUCK_NO);

alter table SHIPMENT add foreign key (DESTINATION) references CITY(CITY_NAME);

alter table SHIPMENT modify WEIGHT int(4) default 10;
alter table SHIPMENT add constraint weight_cont check(WEIGHT<1000);

desc SHIPMENT;
